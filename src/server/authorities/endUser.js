import  Task  from '../model/tasks.model';

module.exports = {
	"updateTaskById": Task.updateTaskById,
  "removeTaskById": Task.removeTaskById
}
