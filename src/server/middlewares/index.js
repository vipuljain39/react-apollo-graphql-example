import authorizeUser from './authorizeUser';

module.exports = {
  authorizeUser: authorizeUser
}
