'use strict';
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    id: String,
    email: String,
    password: String,
    token: String,
    userType: {type: String, default: 'endUser'}
});

module.exports = mongoose.model('Users', UserSchema);
