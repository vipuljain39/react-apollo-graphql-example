//Please add all the resolvers at this file for now
var {PubSub}  =  require('graphql-subscriptions');
const TASK_ADDED = 'addedTask';
var Tasks = require("../model/tasks.model.js");
var Users = require("../model/users.model.js");
import { authorities } from '../authorities';
import { generateToken, authenticateToken, createPasswordHash, validatePassword } from '../common';

var pubsub = new PubSub();

const resolvers = {
    Query: {
        multipleTodos: (root) => {

            var foundItems = new Promise((resolve, reject) => {
                //var projections = getProjection(fieldASTs);
                Tasks.find((err, todos) => {
                    console.log(err,todos);
                    err ? reject(err) : resolve(todos)
                }).exec()
            });
            return foundItems;

        },
        singleTodo: (root,{taskName}) => {

            var foundItems = new Promise((resolve, reject) => {
                //var projections = getProjection(fieldASTs);
                Tasks.find({taskName},(err, todos) => {
                    console.log(err,todos);
                    err ? reject(err) : resolve(todos)
                }).exec()
            });

            return foundItems;

        }
    },
    Mutation: {
        addUser: (root,params) => {
            var taskData = new Tasks(params.newTodo);
            pubsub.publish(TASK_ADDED, taskData)

            return taskData.save();
        },
        updateUser:(root,params)=>{
          return authorities('endUser').updateTaskById(params.id,params.updatedTodo);
        },
        deleteUser: (root,params) => {
          return authorities('endUser').removeTaskById(params.id);
        },
        createUser: async (root, params) => {
            var res = {};
            var total_users = await Users.find().count();
            var dbUser = await Users.findOne({email: params.email});
            if (dbUser) {
                res.token = null;
                throw new Error('User Already Exist');
            }

            var userData = new Users({
              email: params.email,
              password: createPasswordHash(params.password),
              id: total_users+1
            });
            await userData.save(function (err, data) {
                if (err)
                    return err;

                res = data;
            });
            res.token = generateToken({email: params.email, userType: 'endUser'},'24h');
            return res;
        },
        loginUser: async (root, params) => {
            let res = {};
            await Users.findOne({email: params.email}, function (err, data){
                if(err)
                    return err;
                if (data.password) {

                    let pass = data.password;
                    let validPass = validatePassword(data.password, params.password);

                    if (validPass) {
                        res.userId = data.id;
                    }
                    else
                        res.userId = null;
                }
            });
            if (res.userId !== null)
                res.token = generateToken({email: params.email, userType: 'endUser'},'24h');
            else
                res.token = null;

            return res;
        },
    },

    Subscription: {
        addedTask: {
            subscribe:() =>  pubsub.asyncIterator(TASK_ADDED),
            resolve: (payload) => {
                console.log('The payload is ::: ', payload);
                return payload;
            }
        },
    }
};

export default resolvers;
