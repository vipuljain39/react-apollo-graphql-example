
import React, { PropTypes } from 'react';
import { Link,Redirect } from 'react-router-dom';
import { Card, CardText,CardActions, CardHeader } from 'material-ui/Card';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const RegisterForm = ({OnSubmit, OnChange, errors, user,redirectTo}) => (
    <MuiThemeProvider>
        { (redirectTo) ? <Redirect to="/"/> :
            (
                <form onSubmit={ OnSubmit }>
                    <Card className="container" style={{"width" : "350px" , "height":"auto" , "margin" : "150px auto"}}>
                        <CardHeader style={{ "width":"100%","text-align":"center"}} title="SIGNUP"/>
                        {errors.summary && <p className="error-message">{errors.summary}</p>}
                        <CardText>
                            <TextField id="email" name="email"
                                       onChange={OnChange}
                                       value={user.email}
                                       hintText="Enter your Username"
                                       errorText={errors.email}
                                       floatingLabelText="Username"/><br/>
                            <TextField id="password" name="password"
                                       onChange={OnChange}
                                       value={user.password}
                                       type="password"
                                       hintText="Enter your Password"
                                       errorText={errors.password}
                                       floatingLabelText="Password"/><br />
                        </CardText>
                        <CardActions>
                            <RaisedButton type="submit" style={{ "text-align":"center"}} label="SignUP" primary/><br/>
                            <CardText>Already have an account? <Link to="/">LogIn</Link>.</CardText>
                        </CardActions>
                    </Card>
                </form>
            )
        }
    </MuiThemeProvider>
);

RegisterForm.PropTypes = {
    OnSubmit: PropTypes.func.isRequired,
    OnChange: PropTypes.func.isRequired,
    errors: PropTypes.func.isRequired,
    user:PropTypes.func.isRequired,
    redirectTo:PropTypes.func.isRequired,
};

export default RegisterForm