
import React, {Component} from 'react';
import RegisterForm from './RegisterForm';
import {gql,graphql} from 'react-apollo';
import toastr from 'toastr';
class RegisterPage extends Component{
    constructor(props){
        super(props)
        this.state = {
            errors: '',
            user: {},
            redirectTo:false
        }
        this.OnSubmit = this.OnSubmit.bind(this);
        this.OnChange = this.OnChange.bind(this);
    }

    OnSubmit(event){
        event.preventDefault();
        console.log(this.state.user);
        const email = this.state.user.email;
        const password = this.state.user.password;
        this.props.createUser(email,password)
            .then((data) => {

                if(data.error){
                    this.setState({user:{},errors: data.error});
                }else{
                    toastr.clear();
                    toastr.success("DONE");
                    this.setState({user:{},errors:"", redirectTo: true});
                }
            })
            .catch(err => {
                console.error("error");
                this.setState({errors: err});
            });

    }

    OnChange(event){
        const field = event.target.name;
        const user = this.state.user;
        user[field] = event.target.value;
        this.setState({user});
    }
    render(){
        return(
            <div className="App-intro">
            <RegisterForm
                OnSubmit={this.OnSubmit}
                OnChange={this.OnChange}
                errors={this.state.errors}
                user={this.state.user}
                redirectTo={this.state.redirectTo}
            />
                </div>
        );
    }
}


const addUserMutation = gql `
     mutation createUser($email: String!,$password: String!){
         createUser(email:$email,password:$password) {
             email
         }
     }
 `;

const addUserWithMutation = graphql(addUserMutation,{
    props:({mutate}) => ({
        createUser: (email,password) =>
            mutate({
                variables:{email,password}
            })
    })
})(RegisterPage);

export default addUserWithMutation;