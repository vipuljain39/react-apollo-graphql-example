import React from 'react';
import ReactDOM from 'react-dom';
import './assignment/css/index.css';
import {Link} from 'react-router-dom';
import ComponentA from './assignment/Components/ComponentA.js';
import SearchExample from './assignment/Components/SearchExample.js';
import ServiceChooser from './assignment/Components/ServiceChoose.js';
import TimerExample from './assignment/Components/TimerExample.js';

export default class MenuExample extends React.Component{
    constructor(props){
        super(props);
        this.state={childKey:'',focused: 0};
        console.log('dfdfdfd',this.props.items);
        ReactDOM.render(
            <TimerExample keyFromOtherParent={this.state.childKey}/>,document.getElementById('container')
        );
    }
    keyFromChild(newKey) {
        this.setState({childKey:newKey});
    }
    clicked(index){
        this.setState({focused: index});

        if( this.props.items[index]=='SearchExample'){
            ReactDOM.render(
                <SearchExample/>,document.getElementById('container')
            );
        }
        else if( this.props.items[index]=='ServiceChooser'){
            ReactDOM.render(
                <ServiceChooser/>, document.getElementById('container')
            );
        }
        else if( this.props.items[index]=='TimerExample'){
            ReactDOM.render(
                <TimerExample keyFromOtherParent={this.state.childKey}/>,document.getElementById('container')
            );
        }else{
            ReactDOM.render(
                <ComponentA  keyFromOtherParent={this.keyFromChild.bind(this)}/>, document.getElementById('container')
            );
        }
    }
    render() {
        var self = this;
        return (
            <div>
                <div>
                    <p>Please Click on below link to go to Home Page</p>
                    <Link to="/">Home Page</Link>
                </div>

                <ul>{ this.props.items.map(function(m, index){
                    var style = '';
                    if(self.state.focused == index){
                        style = 'focused';
                    }
                    return <li className={style} onClick={self.clicked.bind(self, index)}>{m}</li>;
                }) }
                </ul>
            </div>
        );
    }
};

