import React,{Component} from 'react';
import '../css/index.css';
import ComponentB from './ComponentB.js';

export default class ComponentA extends Component{
    constructor(props){
        super(props);
        this.state={key:''};
    }
    addRow(event){
        this.setState({key:event.target.value});
    }

    changeParentKeyInA(newKey){
        this.setState({key:newKey});
        this.props.keyFromOtherParent(newKey);
    }

    render(){
        return <div>
                <input type="text" value={this.state.key} onChange={this.addRow.bind(this)} placeholder="Enter Key Here"/>
                <ComponentB keyData={this.state.key} changeParentKeyInB={this.changeParentKeyInA.bind(this)}/>
            </div>;
    }
};