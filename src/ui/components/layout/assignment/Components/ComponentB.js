import React,{Component} from 'react';
import ComponentC from './ComponentC.js';
import '../css/index.css';

export default class ComponentB extends Component{
    constructor(props){
        super(props);
        this.state={
            selected:0
        };
    }

    changeParentKeyInB(newKey) {
        this.props.changeParentKeyInB(newKey);
    }

    addValue(index){
       this.setState({selected:index});
    }

    render() {
        var self=this;
        return (
            <div>
                <ul>{
                    ['value1','value2', 'value3', 'value4'].map(function(m, index){
                        var style = '';
                        if(self.state.selected == index){
                            style = 'focused';
                        }
                        return <li className={style} onClick={self.addValue.bind(self,index)}>{m}</li>;
                    })
                    }
                </ul>
                <ComponentC keyData={this.props.keyData} valueData={ ['value1','value2', 'value3', 'value4'][this.state.selected]}
                    changeParentKeyInC={self.changeParentKeyInB.bind(this)}/>
            </div>
        );
    }
};
