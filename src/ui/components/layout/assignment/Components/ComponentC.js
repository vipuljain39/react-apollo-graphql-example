/**
 * Created by vipul on 6/14/17.
 */
import React,{Component} from 'react';
import '../css/index.css';
import ComponentD from "./ComponentD";

export default class ComponentC extends Component{
    constructor(props){
        super(props);
        this.table=[
            {key:'key1',value:'value1'},
            {key:'key2',value:'value2'},
            {key:'key3',value:'value3'}
        ];
    }
    changeParentKeyInC(newKey) {
        this.props.changeParentKeyInC(newKey);
    }

    render() {
        this.table[3]={
            key:this.props.keyData,
            value:this.props.valueData
        };
        var self=this;
        return (
            <div>
               <table>
                   {
                      self.table.map(function(m, index){
                           return <tr><td>{m.key}</td><td>{m.value}</td></tr>;
                       })
                   }
               </table>
                <ComponentD changeParentKeyInD={self.changeParentKeyInC.bind(this)}/>
            </div>
        );
    }
};
