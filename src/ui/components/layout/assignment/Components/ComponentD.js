import React,{Component} from 'react';
import '../css/index.css';

export default class ComponentD extends Component{
    constructor(props){
        super(props);
    }
    changeParentKeyInD(event){
        console.log('props=',JSON.stringify(this.props));
        this.props.changeParentKeyInD(event.target.value);
    }

    render(){
        return <div>
            <input type="text" onChange={this.changeParentKeyInD.bind(this)} placeholder="update parent key here!"/>
        </div>;
    }
};