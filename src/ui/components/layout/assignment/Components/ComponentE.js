/**
 * Created by vipul on 6/15/17.
 */
import React,{Component} from 'react';
import '../css/index.css';

export default class ComponentE extends Component{
    constructor(props){
        super(props);
        this.state={key:this.props.keyFromOtherParent|| '--'};
    }

    changeParentKeyInA(newKey){
        this.setState({key:newKey});
    }

    render(){
        return <div>
            <input type="text" value={'The key from other parents child is '+this.state.key}/>
        </div>;
    }
};