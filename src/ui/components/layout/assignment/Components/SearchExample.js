/**
 * Created by vipul on 6/14/17.
 */
import React,{Component} from 'react';
import '../css/index.css';

var library=[
    { name: 'Backbone.js', url: 'http://documentcloud.github.io/backbone/'},
    { name: 'AngularJS', url: 'https://angularjs.org/'},
    { name: 'jQuery', url: 'http://jquery.com/'},
    { name: 'Prototype', url: 'http://www.prototypejs.org/'},
    { name: 'React', url: 'http://facebook.github.io/react/'},
    { name: 'Ember', url: 'http://emberjs.com/'},
    { name: 'Knockout.js', url: 'http://knockoutjs.com/'},
    { name: 'Dojo', url: 'http://dojotoolkit.org/'},
    { name: 'Mootools', url: 'http://mootools.net/'},
    { name: 'Underscore', url: 'http://documentcloud.github.io/underscore/'},
    { name: 'Lodash', url: 'http://lodash.com/'},
    { name: 'Moment', url: 'http://momentjs.com/'},
    { name: 'Express', url: 'http://expressjs.com/'},
    { name: 'Koa', url: 'http://koajs.com/'},
];

export default class SearchExample extends Component{
    constructor(){
        super();
        this.state={searchString:''};
        this.libraries='';
    }
    handleChange(e){
        this.setState({searchString:e.target.value.trim().toLowerCase()});
    }

    render(){
        var libraries = library;
        var self=this;
        if(this.state.searchString.length > 0){
            libraries = libraries.filter(function(l){
                return l.name.toLowerCase().match( self.state.searchString );
            });
        }
        return <div>
            <input type="text" onChange={this.handleChange.bind(this)} placeholder="Type here" />
            <div >
                { libraries.map(function(l){
                    return <h4 >{l.name} <a href={l.url}>{l.url}</a></h4>
                }) }
            </div>
        </div>;
    }
};
