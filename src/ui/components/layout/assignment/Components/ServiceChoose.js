/**
 * Created by vipul on 6/15/17.
 */
import React,{Component} from 'react';
import '../css/index.css';

var Service = React.createClass({
    getInitialState: function(){
        return { active: false };
    },
    clickHandler: function (){
        var active=!this.state.active;
        this.setState({ active:active });
        this.props.addTotal( active ? this.props.price : -this.props.price );
    },
    render: function(){

        return  <p className={ this.state.active ? 'active' : '' } onClick={this.clickHandler}>
            {this.props.name} <b>${this.props.price.toFixed(2)}</b>
        </p>;
    }
});

var services = [
    { name: 'Web Development', price: 300 },
    { name: 'Design', price: 400 },
    { name: 'Integration', price: 250 },
    { name: 'Training', price: 220 }
];

export default class ServiceChooser extends Component{
    constructor(){
        super();
        this.state={ total: 0,items:services };
    }
    addTotal( price ){
        this.setState( { total: this.state.total + price } );
    }
    render() {
        var self = this;
        var services = this.state.items.map(function(s){
            return (<Service name={s.name} price={s.price} active={s.active} addTotal={self.addTotal.bind(self)} />);
        });
        return <div>
            <h1>Our services</h1>
            <div id="services">
                {services}
                <p id="total">Total <b>${this.state.total.toFixed(2)}</b></p>
            </div>
        </div>;
    }
};