/**
 * Created by vipul on 6/15/17.
 */
import React,{Component} from 'react';
import ComponentE from './ComponentE.js';
import '../css/index.css';

export default class TimerExample extends Component{
    constructor(){
        super();
        this.state={ elapsed: 0, start:Date.now()  };
    }
    componentDidMount(){
        this.timer = setInterval(this.tick.bind(this), 50);
    }
    componentWillUnmount(){
        clearInterval(this.timer);
    }

    tick(){
        this.setState({elapsed: new Date() - this.state.start});
    }

    render() {
        var elapsed = Math.round(this.state.elapsed / 100);
        var seconds = (elapsed / 10).toFixed(1);
        return <div>
                <p>TimerExample was clicked <b>{seconds} seconds</b> ago.</p>
                <ComponentE keyFromOtherParent={this.props.keyFromOtherParent}/>
            </div>;
    }
};